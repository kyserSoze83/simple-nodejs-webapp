FROM node

ADD ./src/ /src/

WORKDIR /src/

RUN npm install

EXPOSE 8080

ENV APPLICATION_INSTANCE=default

CMD node count-server.js
